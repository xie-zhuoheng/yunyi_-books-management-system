import java.text.SimpleDateFormat;
import java.util.Date;

public class test08 {
    public static void main(String[] args) {
        SimpleDateFormat ft= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String start = ft.format(date);
        long l = date.getTime()+5*1000*60*24*60;
        Date date1= new Date(l);
        System.out.println(date1);
        String end = ft.format(date1);
        System.out.println(end);
    }
}
