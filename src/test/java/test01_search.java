import com.yunyi.modules.utils.JDBC.JDBCUtil;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class test01_search {
    public static void main(String[] args) throws SQLException, InstantiationException, IllegalAccessException {
        String sql = "select book_name,author,introduction,sort from books ;";
        JDBCUtil jdbcUtil = new JDBCUtil();
        List<Map> list = jdbcUtil.executeQuery(sql);
        System.out.println(list);
    }
}
