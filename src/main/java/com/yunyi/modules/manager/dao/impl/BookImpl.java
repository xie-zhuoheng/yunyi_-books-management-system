package com.yunyi.modules.manager.dao.impl;

import com.yunyi.modules.manager.dao.BookDao;
import com.yunyi.modules.manager.entity.Book;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class BookImpl implements BookDao {


    @Override
    public List<Map> list() throws IllegalAccessException, SQLException, InstantiationException {
        String sql = "select book_name,book_nowNumber,book_number from books";
        JDBCUtil jdbcUtil1 = new JDBCUtil();
        List<Map> books = jdbcUtil1.executeQuery(sql);
        return books;
    }

    @Override
    public Integer addBook(Book book) throws SQLException {
        String sql = "insert into books(book_name,book_number,book_nowNumber,author,introduction,sort)values(?,?,?,?,?,?);";
        JDBCUtil jdbcUtil2 = new JDBCUtil();
        jdbcUtil2.executeUpdate(sql,
                book.getBook_name(),
                book.getBook_number(),
                book.getBook_number(),
                book.getAuthor(),
                book.getIntroduction(),
                book.getSort());
        return -1;
    }

    @Override
    public Integer editBook(Book book) {
        String sql = "update books set book_name=?,book_number=?,book_nowNumber=?,author=?,introduction=?,sort=?where book_name=?";
        JDBCUtil jdbcUtil3 = new JDBCUtil();
        try {
            jdbcUtil3.executeUpdate(sql, book.getBook_name(), book.getBook_number(),book.getBook_nowNumber(), book.getAuthor(), book.getIntroduction(), book.getSort(), book.getBook_name());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return -1;
    }

    @Override
    public Integer deleteBook(String book_name) {
        String sql = "delete from books where book_name=?;";
        JDBCUtil jdbcUtil4 = new JDBCUtil();
        try {
            jdbcUtil4.executeUpdate(sql, book_name);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return -1;

    }


}