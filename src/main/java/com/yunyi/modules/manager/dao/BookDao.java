package com.yunyi.modules.manager.dao;

import com.yunyi.modules.manager.entity.Book;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface BookDao {
    /*
     查询所有书籍借阅信息
     */
    public List<Map> list() throws InstantiationException, IllegalAccessException, SQLException;

    /*
    添加书籍
     */
    public Integer addBook(Book book) throws SQLException;

    /*
    编辑书籍
     */
    public Integer editBook(Book book);

    /*
    删除书籍
     */
    public Integer deleteBook(String book_name);


}
