package com.yunyi.modules.manager.controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.manager.dao.impl.BookImpl;
import com.yunyi.modules.manager.entity.Book;
import com.yunyi.modules.manager.entity.Response3;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "editServlet", value = "/editServlet")
public class editServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Book book = JsonUtil.generateJson(new Book(), req);
        int b = 0;
        BookImpl bookimpl = new BookImpl();
        b = bookimpl.editBook(book);
        if (b == -1) {
            Response3 abd = new Response3("success","修改成功");
            ObjectMapper objectMapper = new ObjectMapper();
            String json1 = objectMapper.writeValueAsString(abd);
            resp.getWriter().println(json1);
        } else {
            Response3 abc = new Response3("error","修改失败");
            String json2 = JSON.toJSONString(abc);
            resp.getWriter().println(json2);
        }


    }
}
