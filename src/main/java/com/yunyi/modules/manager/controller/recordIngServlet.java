package com.yunyi.modules.manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "recordIngServlet", value = "/recordIngServlet")
public class recordIngServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JDBCUtil jdbcUtil = new JDBCUtil();
        List recordIng = null;
        try {
            recordIng = jdbcUtil.getList(0);
            System.out.println(recordIng);
            JsonUtil.responseJson(recordIng,resp);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
