package com.yunyi.modules.manager.controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.login.entity.LoginResponse;
import com.yunyi.modules.manager.dao.impl.BookImpl;
import com.yunyi.modules.manager.entity.Book;
import com.yunyi.modules.manager.entity.Response3;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "addServlet", value = "/addServlet")
public class addServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a;
        BookImpl bookimpl = new BookImpl();
        Book book = JsonUtil.generateJson(new Book(),req);
        /*Book book1 = new Book(Integer.parseInt(req.getParameter("bookid")),req.getParameter("bookname"),Integer.parseInt(req.getParameter("bookNumber")),req.getParameter("author"),req.getParameter("introduction"),req.getParameter("sort"));*/
        try {
            a = bookimpl.addBook(book);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (a == -1) {
            Response3 abd = new Response3("success","添加成功");
            ObjectMapper objectMapper = new ObjectMapper();
            String json1 = objectMapper.writeValueAsString(abd);
            resp.getWriter().println(json1);
        } else {
            Response3 abc = new Response3("error","添加失败");
            String json2 = JSON.toJSONString(abc);
            resp.getWriter().println(json2);
        }
    }
}

