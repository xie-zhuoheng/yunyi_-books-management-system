package com.yunyi.modules.manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "recordEndServlet", value = "/recordEndServlet")
public class recordEndServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JDBCUtil jdbcUtil = new JDBCUtil();
        List recordEnd = null;
        try {
            String sql = "select user_name,book_name,start,end from record where status = ?;";
            recordEnd = jdbcUtil.executeQuery(sql,1);
            System.out.println(recordEnd);
            JsonUtil.responseJson(recordEnd,resp);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}