package com.yunyi.modules.manager.controller;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "showBooksServlet", value = "/showBooksServlet")
public class showBooksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sql = "select book_name,book_number,book_nowNumber from books ;";
        JDBCUtil jdbcUtil1 = new JDBCUtil();
        List<Map> bookRecord;
        try {
            bookRecord = jdbcUtil1.executeQuery(sql);
            System.out.println(bookRecord);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        JsonUtil.responseJson(bookRecord,resp);
    }
}
