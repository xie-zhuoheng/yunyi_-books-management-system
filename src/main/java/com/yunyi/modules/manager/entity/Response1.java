package com.yunyi.modules.manager.entity;

import com.alibaba.fastjson.annotation.JSONField;

public class Response1 {
    @JSONField(name="user_name",ordinal=1)
    private String user_name;
    @JSONField(name="book_name",ordinal=2)
    private String book_name;
    @JSONField(name="start",ordinal=3)
    private String start;
    @JSONField(name="end",ordinal=4)
    private String end;

    public Response1() {
    }


    public Response1(String user_name, String book_name, String start, String end) {
        this.user_name = user_name;
        this.book_name = book_name;
        this.start = start;
        this.end = end;
    }

    /**
     * 获取
     * @return user_name
     */
    public String getUser_name() {
        return user_name;
    }

    /**
     * 设置
     * @param user_name
     */
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    /**
     * 获取
     * @return book_name
     */
    public String getBook_name() {
        return book_name;
    }

    /**
     * 设置
     * @param book_name
     */
    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    /**
     * 获取
     * @return start
     */
    public String getStart() {
        return start;
    }

    /**
     * 设置
     * @param start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * 获取
     * @return end
     */
    public String getEnd() {
        return end;
    }

    /**
     * 设置
     * @param end
     */
    public void setEnd(String end) {
        this.end = end;
    }

    public String toString() {
        return "Response1{user_name = " + user_name + ", book_name = " + book_name + ", start = " + start + ", end = " + end + "}";
    }
}
