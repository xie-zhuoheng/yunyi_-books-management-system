package com.yunyi.modules.manager.entity;

public class Response3 {
    private String state;
    private String description;


    public Response3() {
    }

    public Response3(String state, String description) {
        this.state = state;
        this.description = description;

    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Response{" +
                "state='" + state + '\'' +
                ", description='" + description + '\'' +
                '}';
    }


}

