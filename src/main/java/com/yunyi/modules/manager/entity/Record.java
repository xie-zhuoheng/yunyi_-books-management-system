package com.yunyi.modules.manager.entity;

public class Record {
    private Integer id;//记录的id
    private Integer book_id;//书籍的id
    private String book_name;//书籍名
    private Integer user_id;//用户的id
    private String user_name;//用户名
    private String start;//借书的时间
    private String end;//还书时间
    private Integer status;//该记录状态“0 在进行;1 完成;
    private Integer borrowNumber;//租借书籍数量


    public Record() {
    }

    public Record(Integer id, Integer book_id, String book_name, Integer user_id, String user_name, String start, String end, Integer status, Integer borrowNumber) {
        this.id = id;
        this.book_id = book_id;
        this.book_name = book_name;
        this.user_id = user_id;
        this.user_name = user_name;
        this.start = start;
        this.end = end;
        this.status = status;
        this.borrowNumber = borrowNumber;
    }

    /**
     * 获取
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取
     * @return book_id
     */
    public Integer getBook_id() {
        return book_id;
    }

    /**
     * 设置
     * @param book_id
     */
    public void setBook_id(Integer book_id) {
        this.book_id = book_id;
    }

    /**
     * 获取
     * @return book_name
     */
    public String getBook_name() {
        return book_name;
    }

    /**
     * 设置
     * @param book_name
     */
    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    /**
     * 获取
     * @return user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * 设置
     * @param user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     * 获取
     * @return user_name
     */
    public String getUser_name() {
        return user_name;
    }

    /**
     * 设置
     * @param user_name
     */
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    /**
     * 获取
     * @return start
     */
    public String getStart() {
        return start;
    }

    /**
     * 设置
     * @param start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * 获取
     * @return end
     */
    public String getEnd() {
        return end;
    }

    /**
     * 设置
     * @param end
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * 获取
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取
     * @return borrowNumber
     */
    public Integer getBorrowNumber() {
        return borrowNumber;
    }

    /**
     * 设置
     * @param borrowNumber
     */
    public void setBorrowNumber(Integer borrowNumber) {
        this.borrowNumber = borrowNumber;
    }

    public String toString() {
        return "Record{id = " + id + ", book_id = " + book_id + ", book_name = " + book_name + ", user_id = " + user_id + ", user_name = " + user_name + ", start = " + start + ", end = " + end + ", status = " + status + ", borrowNumber = " + borrowNumber + "}";
    }
}
