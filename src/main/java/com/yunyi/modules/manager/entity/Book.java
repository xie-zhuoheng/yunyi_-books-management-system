package com.yunyi.modules.manager.entity;
//书籍
public class Book {
    public Book(String book_name, Integer book_number, String author, String introduction, String sort) {
        this.book_name = book_name;
        this.book_number = book_number;
        this.author = author;
        this.introduction = introduction;
        this.sort = sort;
    }

    private Integer book_id;//书籍序号
    private String book_name;//书籍名称
    private Integer book_number;//书籍数量
    private Integer book_nowNumber;//书籍剩余数量
    private String author;//作者
    private String introduction;//介绍

    public Integer getBook_nowNumber() {
        return book_nowNumber;
    }

    public void setBook_nowNumber(Integer book_nowNumber) {
        this.book_nowNumber = book_nowNumber;
    }

    private String sort;//类别

    public Book() {
    }

    public Book(Integer book_id, String book_name, Integer book_number, String author, String introduction, String sort) {
        this.book_id = book_id;
        this.book_name = book_name;
        this.book_number = book_number;
        this.author = author;
        this.introduction = introduction;
        this.sort = sort;
    }

    /**
     * 获取
     *
     * @return book_id
     */
    public Integer getBook_id() {
        return book_id;
    }

    /**
     * 设置
     *
     * @param book_id
     */
    public void setBook_id(Integer book_id) {
        this.book_id = book_id;
    }

    /**
     * 获取
     *
     * @return book_name
     */
    public String getBook_name() {
        return book_name;
    }

    /**
     * 设置
     *
     * @param book_name
     */
    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    /**
     * 获取
     *
     * @return book_number
     */
    public Integer getBook_number() {
        return book_number;
    }

    /**
     * 设置
     *
     * @param book_number
     */
    public void setBook_number(Integer book_number) {
        this.book_number = book_number;
    }

    /**
     * 获取
     *
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * 设置
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * 获取
     *
     * @return introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置
     *
     * @param introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取
     *
     * @return sort
     */


    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * 设置
     */

    @Override
    public String toString() {
        return "Book{" +
                "book_id=" + book_id +
                ", book_name='" + book_name + '\'' +
                ", book_number=" + book_number +
                ", book_nowNumber=" + book_nowNumber +
                ", author='" + author + '\'' +
                ", introduction='" + introduction + '\'' +
                ", sort=" + sort +
                '}';
    }
}
