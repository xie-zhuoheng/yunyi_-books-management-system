package com.yunyi.modules.register.controller;



/*
注册前后端交互类
请求：String email + String password + String username + Int number
返回：String state + String description

注意tomcat版本10版本以后需要Jakarta包，以前是javax包

 */

import com.yunyi.modules.login.entity.LoginResponse;
import com.yunyi.modules.login.entity.User;
import com.yunyi.modules.register.dao.RegisterDao;
import com.yunyi.modules.register.entity.RegisterResponse;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.annotation.WebServlet;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.regex.Pattern;

@WebServlet(name="RegisterServlet",value ="/user_info/register")
public class RegisterServlet extends HttpServlet {
    User user;
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      RegisterDao registerDaoImp = new com.yunyi.modules.register.dao.RegisterDaoImp();
       JsonUtil jsonUtil = new JsonUtil();
       User user = JsonUtil.generateJson(new User(),req); // 注意！！！！！！！！！
        String username = user.getUsername();
        String email = user.getEmail();
        String password = user.getPassword();
        String verifyCode = user.getVerifyCode();
        int number = user.getNumber();
        /*String email = req.getParameter("email");
        String password = req.getParameter("password");
        String username = req.getParameter("username");
        String verifyCode = req.getParameter("verifyCode");
        int number= 7;*/
        if (username == null || "".equals(email.trim())) {
            RegisterResponse registerResponse1 = new RegisterResponse("error", "用户名不能为空");
            jsonUtil.responseJson(registerResponse1, resp);
            return;
        }
        if (email == null || "".equals(email.trim())) {
            System.out.println("邮箱不能为空");
            RegisterResponse registerResponse2 = new RegisterResponse("error", "邮箱不能为空");
            jsonUtil.responseJson(registerResponse2, resp);
            return;
        }
        if(!Pattern.matches("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$", email)){
            System.out.println("邮箱格式不正确");
            RegisterResponse response = new RegisterResponse("error","邮箱格式不正确");
            jsonUtil.responseJson(response, resp);
            return;
        }
        if (password == null || "".equals(password.trim())) {
            System.out.println("密码不能为空");
            RegisterResponse registerResponse3 = new RegisterResponse("error", "密码不能为空");
            jsonUtil.responseJson(registerResponse3, resp);
            return;
        }
        if (verifyCode == null || "".equals(verifyCode)) {
            System.out.println("验证码不能为空");
            LoginResponse loginResponse = new LoginResponse("error", "验证码不能为空", "");
            jsonUtil.responseJson(loginResponse, resp);
        }
        if (verifyCode.equals("123456")) {
            try {
                String json1 = registerDaoImp.register(username, password, email,number);
                PrintWriter out = resp.getWriter();
                out.println(json1);
            } catch (SQLException e) {
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }else
        {
            LoginResponse loginResponse = new LoginResponse("error", "验证码错误", "");
            jsonUtil.responseJson(loginResponse, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
