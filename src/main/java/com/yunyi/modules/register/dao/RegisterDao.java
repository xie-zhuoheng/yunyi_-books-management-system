package com.yunyi.modules.register.dao;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.SQLException;

public interface RegisterDao {
    public String register(String username, String password, String email,int number) throws IllegalAccessException, MessagingException, IOException, SQLException;
}
