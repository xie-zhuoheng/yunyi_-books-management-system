package com.yunyi.modules.register.dao;
/*
注册功能实现类
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.register.entity.RegisterResponse;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterDaoImp implements RegisterDao{

    public String register(String username, String password, String email,int number) throws IllegalAccessException, MessagingException, IOException, SQLException {
        String json = null;
        int p = 0;
        String sql = "select user_name from user";
        JDBCUtil jdbcUtil = new JDBCUtil();
        List<Map> list1 = jdbcUtil.executeQuery(sql);
        String sql2 = "select email from user";
        JDBCUtil jdbcUtil2 = new JDBCUtil();
        List<Map> list2 = jdbcUtil2.executeQuery(sql2);
        Map map1 = new HashMap<>();
        map1.put("user_name", username);
        Map map2 = new HashMap<>();
        map2.put("email", email);
        for (Map map : list1) {
            if (map.equals(map1)) {
                p += 1;
                System.out.println("用户名已存在");
                RegisterResponse registerResponse3 = new RegisterResponse("error", "用户名已存在");
                ObjectMapper objectMapper = new ObjectMapper();
                String json3 = objectMapper.writeValueAsString(registerResponse3);
                json = json3;
            }
        }
        int flag = 0;
        for (Map map3 : list2) {
            if (map3.equals(map2)) {
                flag += 0;
            } else {
                flag += 1;
            }
        }
        if (flag != list2.size()) {
            p += 1;
            System.out.println("邮箱已存在");
            RegisterResponse registerResponse4 = new RegisterResponse("error", "邮箱已存在");
            ObjectMapper objectMapper = new ObjectMapper();
            String json4 = objectMapper.writeValueAsString(registerResponse4);
            json = json4;
        }
        if (p == 0) {
            System.out.println("注册成功");
            RegisterResponse response = new RegisterResponse("success","注册成功");
            String sql1 ="insert into user (user_name,email,password,number) values(?,?,?,?);";
            JDBCUtil jdbcUtil1 = new JDBCUtil();
            jdbcUtil1.executeUpdate(sql1,username,email,password,number);
            ObjectMapper objectMapper = new ObjectMapper();
            String json5 = objectMapper.writeValueAsString(response);
            json = json5;
            System.out.println(json);
            }
        return json;
    }
}
