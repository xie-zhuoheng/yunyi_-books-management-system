package com.yunyi.modules.register.entity;

/*
注册功能响应实体
 */
public class RegisterResponse {
    private String state;
    private String description;

    @Override
    public String toString() {
        return "RegisterResponse{" +
                "state='" + state + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public RegisterResponse(String state, String description) {
        this.state = state;
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
