package com.yunyi.modules.utils.JsonUtil;
/*
Json获取java对象工具类（不会用）
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public  class JsonUtil {
        public static  String getJson (HttpServletRequest request) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8);
            StringBuilder str = new StringBuilder();
            int respInt = inputStreamReader.read();
            while (respInt != -1) {
                str.append((char) respInt);
                respInt = inputStreamReader.read();
            }
            String json = new String(str);
            return json;

        }
    /*
    public StringBuilder getJackon(HttpServletRequest request) throws IOException {

        BufferedReader bufferedReader = request.getReader();
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while((line = bufferedReader.readLine())!=null){
                stringBuilder.append()
        }
    }

     */
        public static <T > void responseJson (T t, HttpServletResponse response) throws IOException {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(t);
            PrintWriter out = response.getWriter();
            out.println(json);
        }
        public static<T> T generateJson(T t, HttpServletRequest request) throws IOException {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = getJson(request);
            T t1 = (T) objectMapper.readValue(json, t.getClass());
            return t1;
        }
}
