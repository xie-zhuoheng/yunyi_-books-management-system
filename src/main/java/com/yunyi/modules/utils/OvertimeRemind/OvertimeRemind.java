package com.yunyi.modules.utils.OvertimeRemind;

import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.sendEmail.EmailSending;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.yunyi.modules.utils.JDBC.JDBCUtil.freeConnection;
/*
逾期提醒的实现方法放在这里了呦
 */
public class OvertimeRemind extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            remind();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public Map getRecordMap() throws SQLException {
        String sql = "select  user_id,end from  record;";
        Connection connection = JDBCUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        Map map = new HashMap();
        while(resultSet.next()) {
            map.put(resultSet.getString("user_id"),resultSet.getObject("end"));
        }
        preparedStatement.close();
        freeConnection(connection);
        return  map;
    }
    public Map getUserMap() throws SQLException {
        String sql = "select  user_id,email from  user;";
        Connection connection = JDBCUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        Map map = new HashMap();
        while(resultSet.next()) {
            map.put(resultSet.getString("user_id"),resultSet.getObject("email"));
        }
        preparedStatement.close();
        freeConnection(connection);
        return  map;
    }
    public void remind() throws SQLException, MessagingException {
        long now = System.currentTimeMillis();
        Map<String,Date> recordMap = getRecordMap();
        Map<String,String> userMap = getUserMap();
        for (String key: recordMap.keySet()) {
            long end =recordMap.get(key).getTime();
            if(end<=now){
                for (String key1:userMap.keySet()) {
                    if(key1.equals(key)){
                        System.out.println("逾期");
                        EmailSending emailSending = new EmailSending();
                        emailSending.sendEmail(userMap.get(key1),"逾期提醒","您的借书超时，请尽快还书！");
                    }
                }
            }
        }
        }
    }

