package com.yunyi.modules.utils.JDBC;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.yunyi.modules.manager.entity.Response1;
import javax.sql.DataSource;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;


/*
Druid连接池工具类，使用时记得改一下properties文件哦
 */


public class JDBCUtil {
    private static DataSource dataSource;

    //初始化数据库配置
    static {
        Properties properties = new Properties();
        InputStream inputStream = JDBCUtil.class.getClassLoader().getResourceAsStream("Druid.properties");
        try {
            properties.load(inputStream);
            dataSource = DruidDataSourceFactory.createDataSource(properties);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //获取数据库连接对象
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    //回收连接对象
    public static void freeConnection(Connection connection) throws SQLException {
        connection.close();
    }

    //非DQL语句封装
    public int executeUpdate(String sql, Object... params) throws SQLException {
        Connection connection = JDBCUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        for (int i = 1; i < params.length+1; i++) {
            preparedStatement.setObject(i, params[i-1]);
        }
        int rows = preparedStatement.executeUpdate();
        preparedStatement.close();
        freeConnection(connection);
        return rows;
    }


    //DQL语句封装(注意：此方法只能查询数据表中的所有数据且查询数据无序，请先完成sql语句编辑)
    public List<Map> executeQuery(String sql,Object... params) throws SQLException {
        Connection connection = JDBCUtil.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        for (int i = 1; i < params.length+1; i++) {
            preparedStatement.setObject(i, params[i-1]);
        }
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Map> list = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()) {
            Map map = new HashMap();
            for (int i = 1; i < columnCount + 1; i++) {
                Object value = resultSet.getObject(i);
                String columnLabel = metaData.getColumnLabel(i);
                map.put(columnLabel, value);
            }
            list.add(map);
        }
        resultSet.close();
        preparedStatement.close();
        freeConnection(connection);
        return list;
    }

    /*
    实现模糊查询的方法
     */
    public List executeQueryByCondition(String name, String author, String book_id,String sort,String book_nowNumber) throws SQLException {
        Connection connection = JDBCUtil.getConnection();
        String sql = "select * from books where 1=1 ";
        StringBuffer stringBuffer = new StringBuffer(sql);
        List l = new ArrayList();
        if (name != null && name != "") {
            stringBuffer.append("and book_name like '%' ? '%' ");
            l.add(name);
        }
        if (author != null && author != "") {
            stringBuffer.append("and author like '%' ? '%' ");
            l.add(author);
        }
        int book_ID = 0;
        if (book_id != null && book_id != "") {
            book_ID = Integer.parseInt(book_id);
            stringBuffer.append("and book_id=?");
            l.add(book_ID);
        }
        if (sort != null && sort != "") {
            stringBuffer.append("and sort like '%' ? '%'");
            l.add(sort);
        }
        int bookNowNum = 0;
        if(book_nowNumber!=null&&book_nowNumber!=""){
            bookNowNum = Integer.parseInt(book_nowNumber);
            stringBuffer.append("and book_nowNumber = ?");
            l.add(bookNowNum);
        }
        PreparedStatement preparedStatement = connection.prepareStatement(stringBuffer.toString());
        for (int i = 1; i <= l.size(); i++) {
            preparedStatement.setObject(i, l.get(i-1));
        }
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Map> list = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while(resultSet.next()){
            Map map = new HashMap();
            for (int i = 1; i < columnCount+1; i++) {
                Object value = resultSet.getObject(i);
                String columnLabel = metaData.getColumnLabel(i);
                map.put(columnLabel,value);
            }
            list.add(map);
        }
        resultSet.close();
        preparedStatement.close();
        freeConnection(connection);
        return  list;
    }
public List getList(int a) throws SQLException {
        String sql = "select user_name,book_name,start,end from record where status=?";
        List<Response1> recordList= new ArrayList<>();
        List<Map> list = executeQuery(sql,a);
        int i = 0;
    for (Map<String,Class<?>> map:list) {
        Response1 res = new Response1();
        i++;
        for (String key: map.keySet()){
            if(key.equals("user_name")){
                res.setUser_name(String.valueOf(map.get(key)));
            }
            if(key.equals("book_name")){
                res.setBook_name(String.valueOf(map.get(key)));
            }
            if(key.equals("start")){
                res.setStart(String.valueOf(map.get(key)));
            }
            if (key.equals("end")){
                res.setEnd(String.valueOf(map.get(key)));
            }
        }
        recordList.add(res);
    }
    return recordList;
}
    public <T>List<T>  transform(List<Map> list,T t) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException, NoSuchFieldException {List<T> returnList = new ArrayList<>();
        Field[] filed = t.getClass().getDeclaredFields();
        Constructor constructor = t.getClass().getConstructor();
        for (Map<String,Class<?>> map:list) {
            T t1 = (T) constructor.newInstance();
            for (String key: map.keySet()) {
                for (int i = 0; i < filed.length; i++) {
                    if(key.equals(filed[i].getName())){
                        Field a = t.getClass().getDeclaredField(key);
                        a.setAccessible(true);
                        a.set(t1,map.get(key));
                    }
                }
            }
            returnList.add(t1);
        }
        return returnList;
    }
}
