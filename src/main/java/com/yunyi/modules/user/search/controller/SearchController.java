package com.yunyi.modules.user.search.controller;
import com.yunyi.modules.user.getBooks.entity.GetBooksResponse;
import com.yunyi.modules.user.search.entity.SearchCondition;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
/*
查询功能前后端交互类
 */
@WebServlet(name = "search",value = "/user_function/search")
public class SearchController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonUtil jsonUtil = new JsonUtil();
       /*SearchCondition searchCondition = JsonUtil.generateJson(new SearchCondition(),req);*/
        String book_name = req.getParameter("book_name");
        SearchCondition searchCondition = new SearchCondition();
        searchCondition.setBook_name(book_name);
        JDBCUtil jdbcUtil = new JDBCUtil();
        try {
            List list = jdbcUtil.executeQueryByCondition(searchCondition.getBook_name(), searchCondition.getBook_number(), searchCondition.getAuthor(), searchCondition.getSort(), searchCondition.getBook_nowNumber());
            if (!list.equals(null)) {
                GetBooksResponse getBooksResponse = new GetBooksResponse("success","搜索成功",list);
                jsonUtil.responseJson(getBooksResponse,resp);
            }else{
                GetBooksResponse getBooksResponse = new GetBooksResponse("error","搜索失败",null);
                jsonUtil.responseJson(getBooksResponse,resp);
            }
            } catch(SQLException e){
                throw new RuntimeException(e);
            }
        }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
