package com.yunyi.modules.user.search.entity;
/*
查询功能实体
 */
public class SearchCondition {
    private String book_name;
    private String book_number;

    public SearchCondition() {
    }

    private String author;
    private String book_nowNumber;
    private  String sort;

    @Override
    public String toString() {
        return "SearchCondition{" +
                "book_name='" + book_name + '\'' +
                ", book_number='" + book_number + '\'' +
                ", author='" + author + '\'' +
                ", book_nowNumber='" + book_nowNumber + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public SearchCondition(String book_name, String book_number, String author, String book_nowNumber, String sort) {
        this.book_name = book_name;
        this.book_number = book_number;
        this.author = author;
        this.book_nowNumber = book_nowNumber;
        this.sort = sort;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public String getBook_number() {
        return book_number;
    }

    public void setBook_number(String book_number) {
        this.book_number = book_number;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBook_nowNumber() {
        return book_nowNumber;
    }

    public void setBook_nowNumber(String book_nowNumber) {
        this.book_nowNumber = book_nowNumber;
    }

    public SearchCondition(String book_name, String book_number, String author, String book_nowNumber) {
        this.book_name = book_name;
        this.book_number = book_number;
        this.author = author;
        this.book_nowNumber = book_nowNumber;
    }
}
