package com.yunyi.modules.user.getBooks.entity;

import java.util.List;
/*
查询书籍响应实体
String state : 状态
String  description : 状态描述
List list : 书籍集合
 */
public class GetBooksResponse {
    private  String state;
    private  String description;
    private List bookList;

    @Override
    public String toString() {
        return "GetBooksResponse{" +
                "state='" + state + '\'' +
                ", description='" + description + '\'' +
                ", bookList=" + bookList +
                '}';
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List getBookList() {
        return bookList;
    }

    public void setBookList(List bookList) {
        this.bookList = bookList;
    }

    public GetBooksResponse(String state, String description, List bookList) {
        this.state = state;
        this.description = description;
        this.bookList = bookList;
    }
}
