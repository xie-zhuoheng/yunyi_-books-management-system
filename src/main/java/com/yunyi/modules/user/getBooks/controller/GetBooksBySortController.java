package com.yunyi.modules.user.getBooks.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.user.getBooks.entity.GetBooksResponse;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
/*
按照类别搜索书籍
请求：String sort:书籍种类
响应：List bookList:书籍列表
 */
@WebServlet(name = "getBooksBySort",value = "/user_function/getBooksBySort")
public class GetBooksBySortController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonUtil jsonUtil = new JsonUtil();
        String json =  jsonUtil.getJson(req);
        ObjectMapper objectMapper1 = new ObjectMapper();
        /*String sort = objectMapper1.readValue(json,String.class);*/
        JDBCUtil jdbcUtil = new JDBCUtil();
        String sort = req.getParameter("sort");
        try {
            List bookList = jdbcUtil.executeQueryByCondition("","","",sort,"");
            if(!bookList.equals(null)) {
                GetBooksResponse getBooksResponse = new GetBooksResponse("success", "查询成功", bookList);
                jsonUtil.responseJson(getBooksResponse,resp);
            }else{
                GetBooksResponse getBooksResponse = new GetBooksResponse("error", "查询失败", null);
                jsonUtil.responseJson(getBooksResponse,resp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
