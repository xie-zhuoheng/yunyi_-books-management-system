package com.yunyi.modules.user.getBooks.controller;
import com.yunyi.modules.user.getBooks.entity.GetBooksResponse;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
/*
查询所有书籍前后端交互类
请求：null
响应：List bookList：书籍列表
 */
@WebServlet(name="getBooks",value = "/user_function/getAllBooks")
public class GetBooksController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JDBCUtil jdbcUtil = new JDBCUtil();
        String sql = "select book_name,book_number from books;";
        try {
           List allBookList = jdbcUtil.executeQuery(sql);
           if (!allBookList.equals(null)){
            GetBooksResponse getBooksResponse = new GetBooksResponse("success","查询成功",allBookList);
            JsonUtil jsonUtil = new JsonUtil();
            jsonUtil.responseJson(getBooksResponse,resp);
           }else{
               GetBooksResponse getBooksResponse = new GetBooksResponse("error", "查询失败", null);
               JsonUtil jsonUtil = new JsonUtil();
               jsonUtil.responseJson(getBooksResponse,resp);
           }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
