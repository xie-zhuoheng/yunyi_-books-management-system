package com.yunyi.modules.user.lend.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yunyi.modules.user.lend.entity.LendInfo;
import java.sql.SQLException;


public interface LendDao {
    public String  lendService(LendInfo book) throws SQLException, JsonProcessingException;
}
