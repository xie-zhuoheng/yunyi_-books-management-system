package com.yunyi.modules.user.lend.entity;
/*
借书响应实体类
 */
public class LendResponse {
    private String state;
    private  String description;

    public LendResponse(String state, String description) {
        this.state = state;
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
