package com.yunyi.modules.user.lend.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.user.lend.entity.LendInfo;
import com.yunyi.modules.user.lend.entity.LendResponse;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
/*
借书功能实现类

 */
public class LendDaoImp implements  LendDao {
    @Override
    public String lendService(LendInfo lendInfo) throws SQLException, JsonProcessingException {
        JDBCUtil jdbcUtil = new JDBCUtil();
        System.out.println();
        String json = null;
        int flag = 0;
        String sql = "select * from books where book_name = ?;";
        List<Map> list = jdbcUtil.executeQuery(sql, lendInfo.getBook_name());
        System.out.println(list);
        for (Map<String, Integer> map : list) {
            for (String key : map.keySet()) {
                if (key.equals("book_nowNumber")) {
                    if (map.get(key) >= lendInfo.getLendNumber()) {
                        int i = map.get(key);
                        int p = i - lendInfo.getLendNumber();
                        String sql02 = "update books set book_nowNumber = ? where book_name = ?;";
                        jdbcUtil.executeUpdate(sql02, p, lendInfo.getBook_name());
                        flag++;
                        System.out.println(flag);
                    } else {
                        ObjectMapper objectMapper = new ObjectMapper();
                        LendResponse lendResponse = new LendResponse("error", "借书数量超过现有数量");
                        json = objectMapper.writeValueAsString(lendResponse);
                    }
                }
                if (key.equals("book_id")) {
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    String start = ft.format(date);
                    long l = date.getTime() + 5 * 1000 * 60 * 24 * 60;
                    Date date1 = new Date(l);
                    String end = ft.format(date1);
                    String sql03 = "insert into record (borrowNumber,user_id,user_name,book_id,book_name,start,end,status) values(?,?,?,?,?,?,?,?);";
                    jdbcUtil.executeUpdate(sql03, lendInfo.getLendNumber(),lendInfo.getUser_id(), lendInfo.getUser_name(),map.get(key), lendInfo.getBook_name(),start, end, 0);
                    flag++;
                    System.out.println(flag);
                }
            }
        }
        if (flag == 2) {
            ObjectMapper objectMapper = new ObjectMapper();
            LendResponse lendResponse = new LendResponse("success", "借书成功");
            json = objectMapper.writeValueAsString(lendResponse);
        }
        return json;
        }
    }
