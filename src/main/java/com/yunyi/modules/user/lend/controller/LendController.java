package com.yunyi.modules.user.lend.controller;
import com.yunyi.modules.user.lend.dao.LendDaoImp;
import com.yunyi.modules.user.lend.entity.LendInfo;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
/*
借书功能实现类
请求：LendInfo lendInfo：被借书的信息
响应：String  json：响应json对象
 */
@WebServlet(name="Lend",value = "/user_function/lend")
public class LendController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonUtil jsonUtil = new JsonUtil();
       /* LendInfo lendInfo = JsonUtil.generateJson(new LendInfo(),req);*/
        String book_name = req.getParameter("book_name");
        Integer lendNumber = Integer.valueOf(req.getParameter("lendNumber"));
        Integer user_id = Integer.valueOf(req.getParameter("user_id"));
        String user_name = req.getParameter("user_name");
        LendInfo lendInfo = new LendInfo(book_name,user_name,lendNumber,user_id);
        System.out.println(lendInfo.toString());
        LendDaoImp lendDaoImp = new LendDaoImp();
        try {
           String json1 = lendDaoImp.lendService(lendInfo);
            PrintWriter out = resp.getWriter();
            out.println(json1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
