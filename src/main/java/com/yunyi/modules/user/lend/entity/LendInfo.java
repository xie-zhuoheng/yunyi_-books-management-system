package com.yunyi.modules.user.lend.entity;
/*
被借书籍的信息实体类
 */
public class LendInfo {
    private String book_name;
    private String user_name;
    private Integer lendNumber;
    private Integer user_id;

    public LendInfo(String book_name, String user_name, Integer lendNumber, Integer user_id) {
        this.book_name = book_name;
        this.user_name = user_name;
        this.lendNumber = lendNumber;
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setLendNumber(Integer lendNumber) {
        this.lendNumber = lendNumber;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public LendInfo() {
    }

    @Override
    public String toString() {
        return "LendInfo{" +
                "book_name='" + book_name + '\'' +
                ", lendNumber=" + lendNumber +
                ", user_id=" + user_id +
                '}';
    }

    public int getLendNumber() {
        return lendNumber;
    }

    public void setLendNumber(int lendNumber) {
        this.lendNumber = lendNumber;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public int getBook_number() {
        return lendNumber;
    }

    public void setBook_number(int book_number) {
        this.lendNumber = book_number;
    }
}