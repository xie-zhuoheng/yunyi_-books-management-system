package com.yunyi.modules.user.returnBook.entity;
/*
还书功能请求实体
 */
public class ReturnInfo {
    private String book_name;
    private Integer returnNumber;
    private  Integer user_id;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public ReturnInfo(String book_name, Integer returnNumber, Integer user_id) {
        this.book_name = book_name;
        this.returnNumber = returnNumber;
        this.user_id = user_id;
    }

    public ReturnInfo() {
    }

    @Override
    public String toString() {
        return "LendBook{" +
                "book_name='" + book_name + '\'' +
                ", lendNumber='" + returnNumber + '\'' +
                '}';
    }

    public int getLendNumber() {
        return returnNumber;
    }

    public void setLendNumber(int lendNumber) {
        this.returnNumber = lendNumber;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public int getBook_number() {
        return returnNumber;
    }

    public void setBook_number(int book_number) {
        this.returnNumber = book_number;
    }


}