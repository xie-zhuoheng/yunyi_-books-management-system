package com.yunyi.modules.user.returnBook.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yunyi.modules.user.lend.entity.LendResponse;
import com.yunyi.modules.user.returnBook.entity.ReturnInfo;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
/*
还书功能实现类
 */
public class ReturnDaoImp implements ReturnDao{
    @Override
    public String returnBook(ReturnInfo returnInfo) throws SQLException, JsonProcessingException {
        JDBCUtil jdbcUtil = new JDBCUtil();
        String json = null;
        int flag=0;
        String sql = "select * from books where book_name = ?;";
        List<Map> list = jdbcUtil.executeQuery(sql,returnInfo.getBook_name());
        for (Map<String ,Integer> map:list) {
            for (String key: map.keySet()) {
                if(key.equals("book_nowNumber")) {
                    int i = map.get(key);
                    int p = i + returnInfo.getLendNumber();
                    String sql02 = "update books set book_nowNumber = ? where book_name = ? ;;";
                    jdbcUtil.executeUpdate(sql02, p,returnInfo.getBook_name());
                    flag++;
                }
                if(key.equals("book_id")){
                    String sql03 = "update record set status = ? where user_id = ? and  book_name =? ;";
                    jdbcUtil.executeUpdate(sql03,1,returnInfo.getUser_id(),returnInfo.getBook_name());
                    flag++;
                }
            }

        }
        if(flag==2){
            ObjectMapper objectMapper = new ObjectMapper();
            LendResponse lendResponse = new LendResponse("success","还书成功");
            json = objectMapper.writeValueAsString(lendResponse);
        }else{
            ObjectMapper objectMapper = new ObjectMapper();
            LendResponse lendResponse = new LendResponse("error","还书失败");
            json = objectMapper.writeValueAsString(lendResponse);
        }
        return json;
    }
}
