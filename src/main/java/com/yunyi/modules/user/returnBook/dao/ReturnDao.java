package com.yunyi.modules.user.returnBook.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yunyi.modules.user.returnBook.entity.ReturnInfo;
import java.sql.SQLException;

public interface ReturnDao {
    String returnBook(ReturnInfo returnInfo) throws SQLException, JsonProcessingException;
}
