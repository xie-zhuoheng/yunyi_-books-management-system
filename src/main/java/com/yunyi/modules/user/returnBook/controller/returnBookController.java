package com.yunyi.modules.user.returnBook.controller;

import com.yunyi.modules.user.returnBook.dao.ReturnDaoImp;
import com.yunyi.modules.user.returnBook.entity.ReturnInfo;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
/*
还书前后端交互类
请求：ReturnInfo returnInfo：被还书籍实体
响应：String json
 */
@WebServlet(name="return",value = "/user_function/return")
public class returnBookController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonUtil jsonUtil = new JsonUtil();
       /* ReturnInfo returnInfo = JsonUtil.generateJson(new ReturnInfo(),req);*/
        String book_name = req.getParameter("book_name");
        Integer returnNumber = Integer.valueOf(req.getParameter("returnNumber"));
        Integer user_id = Integer.valueOf(req.getParameter("user_id"));
        ReturnInfo returnInfo = new ReturnInfo(book_name,returnNumber,user_id);
        ReturnDaoImp returnDaoImp = new ReturnDaoImp();
        try {
            String json1 = returnDaoImp.returnBook(returnInfo);
            PrintWriter out = resp.getWriter();
            out.println(json1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
