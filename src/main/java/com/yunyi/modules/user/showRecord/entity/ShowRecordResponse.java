package com.yunyi.modules.user.showRecord.entity;

import java.util.List;

public class ShowRecordResponse {
    private  String state;
    private String description;
    private  List revordList;

    public ShowRecordResponse() {
    }

    @Override
    public String toString() {
        return "ShowRecordResponse{" +
                "state='" + state + '\'' +
                ", description='" + description + '\'' +
                ", revordList=" + revordList +
                '}';
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List getRevordList() {
        return revordList;
    }

    public void setRevordList(List revordList) {
        this.revordList = revordList;
    }

    public ShowRecordResponse(String state, String description, List revordList) {
        this.state = state;
        this.description = description;
        this.revordList = revordList;
    }
}
