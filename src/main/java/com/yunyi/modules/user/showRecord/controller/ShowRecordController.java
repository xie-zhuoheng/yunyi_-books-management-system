package com.yunyi.modules.user.showRecord.controller;
import com.yunyi.modules.utils.JDBC.JDBCUtil;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
/*
查询借书记录前后端交互类
请求：Integer user_id
响应：List recordList
 */
@WebServlet(name = "showRecord",value = "/user_function/showRecord")
public class ShowRecordController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonUtil jsonUtil = new JsonUtil();
        /*String json =  jsonUtil.getJson(req);
        ObjectMapper objectMapper1 = new ObjectMapper();
        String user_name = objectMapper1.readValue(json,String.class);*/
        String user_name = req.getParameter("username");
        String sql = "select * from record where user_name = ?;";
        JDBCUtil jdbcUtil = new JDBCUtil();
        try {
            List recordList = jdbcUtil.executeQuery(sql,user_name);
            jsonUtil.responseJson(recordList,resp);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
