package com.yunyi.modules.login.entity;
/*
用户实体
 */
public class User {
    //ID
    private int user_id;
    private int number;
    private  String username;
    private  String password;
    private String email;
    private  String photo;
    private String verifyCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + user_id +
                ", number=" + number +
                ", user_name='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", photo='" + photo + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                '}';
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getUser_name() {
        return username;
    }

    public void setUser_name(String user_name) {
        this.username = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public User() {
    }

    public User(int id, int number, String user_name, String password, String email, String photo, String verifyCode) {
        this.user_id= id;
        this.number = number;
        this.username = user_name;
        this.password = password;
        this.email = email;
        this.photo = photo;
        this.verifyCode = verifyCode;
    }
}
