package com.yunyi.modules.login.entity;

import com.alibaba.fastjson.annotation.JSONType;

/*
登录返回json类
 */
public class LoginResponse {
    private String state;
    private  String description;
    private  String token;

    public LoginResponse(String state, String description, String token) {
        this.state = state;
        this.description = description;
        this.token = token;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Response{" +
                "state='" + state + '\'' +
                ", description='" + description + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
