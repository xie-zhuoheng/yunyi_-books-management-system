package com.yunyi.modules.login.dao;

import java.sql.SQLException;
/*
登录功能实现接口
 */
public interface LoginDao {
   boolean login(String email, String password) throws SQLException, InstantiationException, IllegalAccessException;
}
