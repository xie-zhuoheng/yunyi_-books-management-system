package com.yunyi.modules.login.dao;
/*
登录功能实现类
 */
import com.yunyi.modules.utils.JDBC.JDBCUtil;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginDaoImp implements LoginDao {

    @Override
    public boolean login(String email, String password) throws SQLException, InstantiationException, IllegalAccessException {
        boolean a = false;
        String sql = "select email,password from user";
        JDBCUtil jdbcUtil = new JDBCUtil();
        HashMap<String,String > map1 = new HashMap<>();
        map1.put("email",email);
        map1.put("password",password);
        List<Map> list = jdbcUtil.executeQuery(sql);
            for (int i = 0; i < list.size(); i++) {
                Map<String, String> map = list.get(i);
              if(map.equals(map1)){
                 a = true;
              }
            }
        return  a;
    }
}
