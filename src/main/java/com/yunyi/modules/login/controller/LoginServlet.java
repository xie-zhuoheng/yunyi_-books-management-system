package com.yunyi.modules.login.controller;
/*
登录前后端交互类
请求：email + password
返回：state + description + token

注意tomcat版本10版本以后需要Jakarta包，以前是javax包

 */
import com.yunyi.modules.login.dao.LoginDaoImp;
import com.yunyi.modules.login.entity.LoginResponse;
import com.yunyi.modules.login.entity.User;
import com.yunyi.modules.utils.JsonUtil.JsonUtil;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.websocket.Session;
import java.io.*;
import java.sql.SQLException;
import java.util.Date;
@WebServlet(name="LoginServlet",value ="/user_info/login")
public class LoginServlet extends HttpServlet {
    String token;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        JsonUtil jsonUtil = new JsonUtil();
       User user = JsonUtil.generateJson(new User(),req); // 注意！！！！！！！！！
        String email = user.getEmail();
        String password = user.getPassword();
       /*String email = req.getParameter("email");
        String password = req.getParameter("password");
        String verifyCode = req.getParameter("verifyCode");*/
        if (email == null || "".equals(email.trim())) {
            System.out.println("邮箱不能为空");
            LoginResponse loginResponse1 = new LoginResponse("error", "邮箱不能为空", "");
            jsonUtil.responseJson(loginResponse1, resp);
            return;
        }
        if (password == null || "".equals(password.trim())) {
            System.out.println("密码不能为空");
            LoginResponse loginResponse2 = new LoginResponse("error", "密码不能为空", "");
            jsonUtil.responseJson(loginResponse2, resp);
            return;
        }
        if (email.equals("管理员") && password.equals("123456")) {
            System.out.println("管理员登录");
            LoginResponse loginResponse3 = new LoginResponse("success", "管理员登录成功", "");
            jsonUtil.responseJson(loginResponse3, resp);
        }
        LoginDaoImp loginDaoImp = new LoginDaoImp();
        try {
            if (loginDaoImp.login(email, password)) {
                /*long now = System.currentTimeMillis();
                long exp = now + 1000 * 60 * 60 * 24;
                JwtBuilder jwtBuilder = Jwts.builder()
                        .setId("****")
                        .setSubject("++++")
                        .setIssuedAt(new Date())
                        .signWith(SignatureAlgorithm.HS256, "123456789")
                        .setExpiration(new Date(exp));
                token = jwtBuilder.compact();*/
                LoginResponse loginResponse4 = new LoginResponse("success", "登录成功", "yes");
                jsonUtil.responseJson(loginResponse4, resp);
                HttpSession session = req.getSession();
                session.getAttribute(user.getUser_name());
                System.out.println(loginResponse4);
            } else {
                LoginResponse loginResponse5 = new LoginResponse("error", "邮箱或密码错误", "");
                jsonUtil.responseJson(loginResponse5, resp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        }
}


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        doPost(req, resp);
    }
}
